package ru.itis.homeworkone.app;

import com.beust.jcommander.converters.IParameterSplitter;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class ColonParameterSplitter implements IParameterSplitter {

    @Override
    public List<String> split(String s) {
        return asList(s.split(","));
    }
}
