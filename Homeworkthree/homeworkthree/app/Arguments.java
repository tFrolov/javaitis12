package ru.itis.homeworkone.app;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.ArrayList;
import java.util.List;
@Parameters(separators = "=")
public class Arguments {
    @Parameter(
    names={"--file","-f"},
    description = "Id of the Customer who's using the services",
    splitter = ColonParameterSplitter.class
    )

    private List<String> files = new ArrayList<>();

    public List<String> getFiles(){
        return this.files;
    }
}

