package ru.itis.homeworkone.app;
import com.beust.jcommander.JCommander;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import ru.itis.homeworkone.utils.FileReaderThread;
import ru.itis.homeworkone.utils.MyFileVisitor;
import ru.itis.homeworkone.utils.PrintFiles;
/*
print file path
 */
public class Program {
    static{
        String username = System.getProperty("user.name");
        System.out.println("\tHello " + username);
        System.out.println("\tPlease provide argument - folder name or file name \r\n Parameter --file , separator is ,");

    }
    public static void main(String[] args) {
//        String[] argv = { "-f", "C:\\", "-f", "C:\\Users\\TimurFrolov\\Desktop\\Timur\\dataSource\\", "-f", "C:\\Users\\TimurFrolov\\Desktop\\Timur\\dataSource\\", "-f", "C:\\Users\\TimurFrolov\\Desktop\\Timur\\dataSource\\" };
        Arguments jArgs = new Arguments();
        JCommander.newBuilder()
                .addObject(jArgs)
                .build()
                .parse(args);
        List<String> filesList = jArgs.getFiles();

        Thread thread;
        List<Thread> treadList = new ArrayList<>();
//        System.out.println(filesList.toString());
        for(int i = 0; i < filesList.size();i++) {
            thread = new Thread(new FileReaderThread(filesList.get(i)));
            thread.start();
            treadList.add(thread);
        }

        for(Thread tread: treadList){
            try {
                tread.join();

            }catch(InterruptedException e){
                System.out.println("закрылся");
                throw new IllegalArgumentException();
            }
        }


        }
    }

