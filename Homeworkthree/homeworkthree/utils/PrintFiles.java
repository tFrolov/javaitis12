package ru.itis.homeworkone.utils;

import java.io.IOException;
import java.nio.file.*;
import java.util.HashSet;


public class PrintFiles {

    public static void printFiles(String filePath) throws IOException{
        Path desiredPath = Paths.get(filePath);
        Files.walkFileTree(desiredPath, new HashSet<FileVisitOption>(),1,new MyFileVisitor());

    }
}
