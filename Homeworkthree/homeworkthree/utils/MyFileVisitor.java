package ru.itis.homeworkone.utils;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

public class MyFileVisitor extends SimpleFileVisitor<Path> {
    private static final List<String> listFiles = new ArrayList<>();

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
//        PrintStream ps = new PrintStream(System.out, true, "UTF-8");
//        ps.println(file.getFileName());
        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        fmt.format("FILE NAME : <%s> AND ITS SIZE  <%s> IN BYTES",file.getFileName(),Files.size(file));
        listFiles.add(sbuf.toString());
//        System.out.printf("Название файла: %s , размер файла %s в байтах \r\n",file.getFileName(), Files.size(file));
        return FileVisitResult.CONTINUE;
    }

    public static List<String> getListFiles (){
        return listFiles;
    }

}
