package ru.itis.homeworkone.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileReaderThread implements Runnable {
    private String name;
    private Map<String,List<String>> filesMap = new HashMap<>();
//    private final List<String> filesList;

    public FileReaderThread(String fileName) {
        this.name = fileName;
    }
    public Map<String,List<String>> getFilesMap(){
        return filesMap;
    }

    @Override
    public void run(){
        try {
            PrintFiles.printFiles(name);
            filesMap.put(name,MyFileVisitor.getListFiles());
            System.out.println(filesMap.toString());
        }catch(IOException e){
            System.out.println("You provided bad data, try again");
            e.printStackTrace();
        }
    }

}
