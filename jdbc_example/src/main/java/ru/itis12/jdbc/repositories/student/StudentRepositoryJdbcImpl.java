package ru.itis12.jdbc.repositories.student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ru.itis12.jdbc.model.Student;
import ru.itis12.jdbc.repositories.AbstractSqlRepository;
import ru.itis12.jdbc.repositories.RowMapper;

public class StudentRepositoryJdbcImpl extends AbstractSqlRepository<Integer, Student> {
	private Connection connection;
	
	//language = sql
	private static final String SQL_SELECT_ALL = "Select * from student";
	private static final String SQL_SELECT_BY_ID = "Select * from student where id = ";
//	public StudentRepositoryJdbcImpl (Connection connection) {
//		this.connection = connection;
//	}
	
	
	private RowMapper<Student> studentRowMapper = new RowMapper<Student>() {
		public Student mapRow(ResultSet row) throws SQLException{
			return new Student(
					row.getInt("id"),
					row.getString("first_name"),
					row.getString("last_name"),
					row.getInt("age"),
					row.getBoolean("is_active")
					);
		}
	};

	
	
	public StudentRepositoryJdbcImpl(Connection connection) {
		this.connection = connection;
		
	}

	public Student find(Integer id) {
		try {
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(SQL_SELECT_BY_ID + id);
			result.next();
			return studentRowMapper.mapRow(result);
		}catch(SQLException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public List<Student> findAll() {
		try{
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
			List<Student> result = new ArrayList<Student>();
			
			while(resultSet.next()) {
				Student student = studentRowMapper.mapRow(resultSet);
				result.add(student);
			}
			return result;
		}catch(SQLException e) {
			new IllegalArgumentException(e);
			System.out.println("what");
		}
		
		return null;
	}
	public void save(Student object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Student findEntityById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean create(Student entity) {
		// TODO Auto-generated method stub
		return false;
	}

	public void update(Student object) {
		// TODO Auto-generated method stub
		
	}

	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}

	

	public void insert(Student object) {
		// TODO Auto-generated method stub
		
	}



}
