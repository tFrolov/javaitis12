package ru.itis12.jdbc.repositories.lesson;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ru.itis12.jdbc.model.Course;
import ru.itis12.jdbc.model.Lesson;
import ru.itis12.jdbc.repositories.AbstractSqlRepository;
import ru.itis12.jdbc.repositories.RowMapper;

public class LessonRepository extends AbstractSqlRepository<Integer, Lesson> {
	private static final String SQL_SELECT_ALL = "Select * from lesson";
	private static final String SQL_SELECT_BY_ID = "Select * from lesson where id = ";
	
	private RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
		public Lesson mapRow(ResultSet row) throws SQLException{
			return new Lesson(
					row.getInt("Course_id"),
					row.getString("name"),
					row.getInt("id")
					);
		}
	};
	public LessonRepository() {
		super();
		// TODO Auto-generated constructor stub
	}


	@Override
	public List<Lesson> findAll() {
		try {
			List<Lesson> list = new ArrayList<Lesson>();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);	
			while(resultSet.next()) {
				Lesson lesson = lessonRowMapper.mapRow(resultSet);
				list.add(lesson);
			}
			return list;
		}catch(SQLException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public Lesson findEntityById(Integer id) {
		try {
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(SQL_SELECT_BY_ID + id);
			result.next();
			return lessonRowMapper.mapRow(result);
		}catch(SQLException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public boolean create(Lesson entity) {
		// TODO Auto-generated method stub
		return false;
	}
	public void insert(Lesson object) {
		// TODO Auto-generated method stub
		
	}

	public void save(Lesson object) {
		// TODO Auto-generated method stub
		
	}

	public void update(Lesson object) {
		// TODO Auto-generated method stub
		
	}

	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}
	
}
