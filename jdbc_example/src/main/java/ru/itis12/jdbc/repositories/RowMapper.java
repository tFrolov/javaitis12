package ru.itis12.jdbc.repositories;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowMapper<T> {
	T mapRow(ResultSet row) throws SQLException;
}
