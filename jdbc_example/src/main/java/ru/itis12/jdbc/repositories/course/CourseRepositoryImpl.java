package ru.itis12.jdbc.repositories.course;

import java.util.ArrayList;
import java.util.List;


import ru.itis12.jdbc.model.Course;
import ru.itis12.jdbc.model.Student;
import ru.itis12.jdbc.repositories.AbstractSqlRepository;
import ru.itis12.jdbc.repositories.RowMapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CourseRepositoryImpl extends AbstractSqlRepository<Integer, Course>{
	private static final String SQL_SELECT_ALL = "Select * from course";
	private static final String SQL_SELECT_BY_ID = "Select * from course where id = ";
	private RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
		public Course mapRow(ResultSet row) throws SQLException{
			return new Course(
					row.getDate("finish_date"),
					row.getDate("start_date"),
					row.getString("title"),
					row.getInt("id")
					);
		}
	};


	public CourseRepositoryImpl(Connection connection) {
		super(connection);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Course> findAll() {
		try {
			List<Course> list = new ArrayList<Course>();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);	
			while(resultSet.next()) {
				Course course = courseRowMapper.mapRow(resultSet);
				list.add(course);
			}
			return list;
		}catch(SQLException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public Course findEntityById(Integer id) {
		try {
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(SQL_SELECT_BY_ID + id);
			result.next();
			return courseRowMapper.mapRow(result);
		}catch(SQLException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public boolean create(Course entity) {
		// TODO Auto-generated method stub
		return false;
	}
	public void insert(Course object) {
		// TODO Auto-generated method stub
		
	}

	public void save(Course object) {
		// TODO Auto-generated method stub
		
	}

	public void update(Course object) {
		// TODO Auto-generated method stub
		
	}

	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}
	

}
