package ru.itis12.jdbc.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ru.itis12.jdbc.model.Student;

public abstract class AbstractSqlRepository<K extends Number, T> implements CrudRepository<K,T> {
	public Connection connection;

	protected AbstractSqlRepository(Connection connection){
		this.connection = connection;
	}
	protected AbstractSqlRepository(){
		
	}
	public abstract List<T> findAll();

    public abstract T findEntityById(K id);


    public abstract boolean create(T entity);

}
