package ru.itis12.jdbc.repositories;

import java.util.List;

import ru.itis12.jdbc.model.Course;

import java.util.ArrayList;
//create reader update delete
public interface CrudRepository<K,T> {
	void insert(T object);
	void save(T object);
	void update(T object);
	void delete(K id);
	
}
