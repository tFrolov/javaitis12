package ru.itis12.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import ru.itis12.jdbc.connector.ConnectorDataBase;
import ru.itis12.jdbc.model.Student;
import ru.itis12.jdbc.repositories.course.CourseRepositoryImpl;
import ru.itis12.jdbc.repositories.student.StudentRepositoryJdbcImpl;

public class MainForRepositories  {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
//		Class.forName("org.postgresql.Driver");
		Connection connection = ConnectorDataBase.getConnection();
		StudentRepositoryJdbcImpl repository = new StudentRepositoryJdbcImpl(connection);
		Student marat = repository.find(2);
		System.out.println(marat);
		List<Student> listStudent = repository.findAll();
		for(Student st : listStudent) {
			System.out.println(st);
		}
		Connection connectionCourse = ConnectorDataBase.getConnection();
		CourseRepositoryImpl courseRepository = new CourseRepositoryImpl(connectionCourse);
		
	}

}
