package ru.itis12.jdbc.model;

import java.util.Date;

public class Course {
	private Date finishDate;
	private Date startDate;
	private String title;
	private Integer id;
	
	public Course(Date finishDate, Date startDate, String title, Integer id) {
		super();
		this.finishDate = finishDate;
		this.startDate = startDate;
		this.title = title;
		this.id = id;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	
	public Date getStartDate() {
		return startDate;
	}


	public String getTitle() {
		return title;
	}

	public Integer getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Course [finishDate=" + finishDate + ", startDate=" + startDate + ", title=" + title + ", id=" + id
				+ "]";
	}


	
	
}
