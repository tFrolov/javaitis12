package ru.itis12.jdbc.model;

public class Lesson {
	private Integer course_id;
	private String name;
	private Integer id;
	
	
	
	public Lesson(Integer course_id, String name, Integer id) {
		super();
		this.course_id = course_id;
		this.name = name;
		this.id = id;
	}



	public Integer getCourse_id() {
		return course_id;
	}



	public void setCourse_id(Integer course_id) {
		this.course_id = course_id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	@Override
	public String toString() {
		return "Lesson [course_id=" + course_id + ", name=" + name + ", id=" + id + "]";
	}
	

}
